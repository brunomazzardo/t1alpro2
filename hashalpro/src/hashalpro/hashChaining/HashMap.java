package hashalpro.hashChaining;

import java.util.ArrayList;

import hashalpro.front.Item;

public class HashMap {
	private final static int TABLE_SIZE = 13;

	LinkedHashEntry[] table;

	public HashMap() {
		table = new LinkedHashEntry[TABLE_SIZE];
		setTableNull();
	}
	public void setTableNull(){
		for (int i = 0; i < TABLE_SIZE; i++)
			table[i] = null;
		
	}

	public ArrayList<Item> listarTodas() {
		int count = 0;
		ArrayList<Item> it = new ArrayList<>();

		while (count < TABLE_SIZE) {
			LinkedHashEntry entry = table[count];
			while (entry != null) {
				if (entry != null) {
					it.add(entry.getValue());
				}

				entry = entry.getNext();
			}
			count++;

		}
		return it;

	}

	public Item get(int key) {
		int hash = (key % TABLE_SIZE);
		if (table[hash] == null)
			return null;
		else {
			LinkedHashEntry entry = table[hash];
			while (entry != null && entry.getKey() != key)
				entry = entry.getNext();
			if (entry == null)
				return null;
			else
				return entry.getValue();
		}
	}

	public Item getNome(String nome) {
		int count = 0;

		while (count < TABLE_SIZE) {
			LinkedHashEntry entry = table[count];
			while (entry != null) {
				
				if (entry != null) {
					if (entry.getValue().getNome().equalsIgnoreCase(nome)) {
						return entry.getValue();
					}
				}
				entry = entry.getNext();
			}
			count++;

		}
		return null;

	}

	public boolean put(Item value) {
		try {
			if (get(value.getCodigo()) != null) {
				return false;

			}
			int hash = (value.getCodigo() % TABLE_SIZE);
			if (table[hash] == null)
				table[hash] = new LinkedHashEntry(value);
			else {
				LinkedHashEntry entry = table[hash];
				while (entry.getNext() != null && entry.getKey() != value.getCodigo())
					entry = entry.getNext();
				if (entry.getKey() == value.getCodigo())
					entry.setValue(value);
				else
					entry.setNext(new LinkedHashEntry(value));
			}
			return true;
		} catch (Exception E) {
			return false;
		}
	}

	public Item maisBarato() {
		int count = 0;
		Item menor = new Item("void", 0, 0, 9999999999999.9);
		while (count < TABLE_SIZE) {
			LinkedHashEntry entry = table[count];
			while (entry != null) {

				if (entry != null) {
					if (entry.getValue().getPreco() < menor.getPreco()) {
						menor = entry.getValue();
					}
				}
				entry = entry.getNext();
			}
			count++;

		}
		return menor;

	}

	public void remove(int key) {
		int hash = (key % TABLE_SIZE);
		if (table[hash] != null) {
			LinkedHashEntry prevEntry = null;
			LinkedHashEntry entry = table[hash];
			while (entry.getNext() != null && entry.getKey() != key) {
				prevEntry = entry;
				entry = entry.getNext();
			}
			if (entry.getKey() == key) {
				if (prevEntry == null)
					table[hash] = entry.getNext();
				else
					prevEntry.setNext(entry.getNext());
			}
		}
	}
}