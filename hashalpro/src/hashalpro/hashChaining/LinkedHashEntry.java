package hashalpro.hashChaining;

import hashalpro.front.Item;

public class LinkedHashEntry {
    private int key;
    private Item itemValue;
    private LinkedHashEntry next;

    LinkedHashEntry( Item value) {
          this.key =value.getCodigo();
          this.itemValue = value;
          this.next = null;
    }

    public Item getValue() {
          return itemValue;
    }

    public void setValue(Item value) {
          this.itemValue = value;
    }

    public int getKey() {
          return key;
    }

    public LinkedHashEntry getNext() {
          return next;
    }

    public void setNext(LinkedHashEntry next) {
          this.next = next;
    }
}