package hashalpro.front;

import java.util.InputMismatchException;
import java.util.Scanner;

import hashalpro.hashChaining.HashMap;

public class app {
	public static Scanner in = new Scanner(System.in);
	public static geraCodigo gc = new geraCodigo();
	public static HashMap hm = new HashMap();

	public static void main(String args[]) {

		hm.put(new Item("Bola", gc.geraCodigoUnico(), 1, 33.0));
		hm.put(new Item("Tenis", gc.geraCodigoUnico(), 2, 22.0));
		hm.put(new Item("Raquete", gc.geraCodigoUnico(), 1, 14.0));
		hm.put(new Item("Meia", gc.geraCodigoUnico(), 2, 5.0));
		hm.put(new Item("Rede", gc.geraCodigoUnico(), 1, 233.0));
		hm.put(new Item("Chuteira", gc.geraCodigoUnico(), 2, 16.0));
		// System.out.println(hm.get(1).toString());
		menu();
	}

	public static void menu() {
		int escolha = 0;
		System.out.println("1 para adicionar item" + "\n" + "2 para adicionar com codigo unico" + "\n"
				+ "3 para procurar produto pelo nome" + "\n" + "4 para procurar pelo codigo" + "\n"
				+ "5 para listar todos" + "\n" + "6 para o mais barato" + "\n" + "7 para remover pelo codigo");
		try {
			escolha = in.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("Somente Numeros");
		}
		switch (escolha) {
		case 1:
			adiconaItem();
			menu();
			break;
		case 2:
			adiconaItemUnico();
			menu();

			break;
		case 3:
			procuraPeloNome();
			menu();
			break;
		case 4:
			procuraPeloCodigo();
			menu();

			break;
		case 5:
			System.out.println("Nome \tCodigo \tSetor \tPre�o");
			for (Item i : hm.listarTodas()) {
				System.out.println(i.getNome() + "\t" + i.getCodigo() + "\t" + i.getSetor() + "\t" + i.getPreco());
			}

			menu();
			break;

		case 6:
			System.out.println(hm.maisBarato().toString());
			menu();
			break;
		case 7:
			remove();
			menu();
			break;
		default:
			System.out.println("Abra�o");

		}
	}

	public static void remove() {
		System.out.println("Digita o Codigo do produto");
		try {
			int nome = in.nextInt();

			hm.remove(nome);
		} catch (InputMismatchException S) {
			System.out.println("Apenas Numeros");

		} catch (Exception E) {
			System.out.println("N�o achou");
		}
	}

	public static void procuraPeloCodigo() {
		System.out.println("Digita o Codigo do produto");
		try {
			int nome = in.nextInt();
			System.out.println(hm.get(nome).toString());
		} catch (InputMismatchException S) {
			System.out.println("Apenas Numeros");

		} catch (Exception E) {
			System.out.println("Produto n�o encontrado");
		}
	}

	public static void procuraPeloNome() {
		System.out.println("Digita o nome do produto");
		String nome = in.next();
		try {
			System.out.println(hm.getNome(nome).toString());
		} catch (Exception E) {
			System.out.println("Produto n�o encontrado");
		}
	}

	public static void adiconaItemUnico() {
		try {
			System.out.println("Digite Nome,setor e Valor");

			String nome = in.next();

			int setor = in.nextInt();
			double Valor = in.nextDouble();
			if (hm.put(new Item(nome, gc.geraCodigoUnico(), setor, Valor))) {
				System.out.println("Adicionado com Sucesso");
			}

		} catch (Exception e) {
			System.out.println("Erro na digita��o");

		}
	}

	public static void adiconaItem() {
		try {
			System.out.println("Digite Nome,Codigo,setor e Valor");

			String nome = in.next();
			int codigo = in.nextInt();
			int setor = in.nextInt();
			double Valor = in.nextDouble();
			if (hm.put(new Item(nome, codigo, setor, Valor))) {
				System.out.println("Adicionado com Sucesso");
			} else {
				System.out.println("Codigo Invalido");
			}

		} catch (Exception e) {
			System.out.println("Erro na digita��o");

		}
	}

}
