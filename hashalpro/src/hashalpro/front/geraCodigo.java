package hashalpro.front;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class geraCodigo {
	private ArrayList<Integer> randNum = new ArrayList<>();

	public geraCodigo() {
		for (int i = 0; i < 300; i++) {
			randNum.add(i, i);
		}

	}

	public int geraCodigoUnico() {

		Collections.shuffle(randNum);

		Integer temp = randNum.get(1);
		randNum.remove(1);
		return temp;

	}

}
